import moment from 'moment/moment'
import React from 'react'
import "moment"


export default function MovieItemTab({movie}) {
  return (
    <div className='flex mt-10 space-x-5'>
        <img className='h-40 w-28 object-cover' src={movie.hinhAnh} alt="" />
        <div>
            <h3 className='font-medium text-2xl'>{movie.tenPhim}</h3>
            <div className='grid grid-cols-3 gap-7 mt-5'>
                {movie.lstLichChieuTheoPhim.slice(0,6).map((lichChieu) => {
                    return (
                        <button className='p-3  shadow-lg bg-zinc-100 text-green-500'>{moment(lichChieu.ngayChieuGioChieu).format("DD/MM~hh:mm A")}</button>
                    )
                })}
            </div>
        </div>
    </div>
  )
}
