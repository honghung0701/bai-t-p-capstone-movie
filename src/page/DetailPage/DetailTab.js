import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import "moment";
import { useParams } from "react-router-dom";
import { getMovieInfor } from "./../../service/movieService";
import moment from 'moment/moment'

// import "moment/locale/vi"

// moment.locale("vi")

export default function DetailTab() {
  let param = useParams();
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    getMovieInfor(param.id)
      .then((res) => {
        console.log(res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const onChange = (key) => {
    console.log(key);
  };
  const renderHeThongRap = () => {
    return dataMovie.heThongRapChieu?.map((heThongRap) => {
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            items={renderThongTinRap(heThongRap)}
            onChange={onChange}
          />
        ),
      };
    });
  };
  const renderThongTinRap = (heThongRap) => {
    return heThongRap?.cumRapChieu?.map((thongTinRap) => {
      return {
        label: (
          <div className="w-44">
            <h3>{thongTinRap.tenCumRap}</h3>
            <p className="truncate">{thongTinRap.diaChi}</p>
            <p className="text-red-600">[Chi Tiết]</p>
          </div>
        ),
        key: thongTinRap.maCumRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            items={renderLichChieuPhim(thongTinRap)}
            onChange={onChange}
          />
        ),
      };
    });
  };
  const renderLichChieuPhim = (thongTinRap) => {
    return thongTinRap.lichChieuPhim.map((lichChieu) => {
      return {
        label: (
          <div className="flex items-center mt-10 space-x-5">
            <h3 className="font-black">Lịch Chiếu Phim:</h3>
            <button className='p-3  shadow-lg bg-zinc-100 text-green-500'>              {moment(lichChieu.ngayKhoiChieu).format("LLLL")}

</button>
          </div>
        ),
        key: lichChieu.maLichChieu,
      };
    });
  };
  return (
    <div className="mx-auto">
      <Tabs
        style={{
          height: 400,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
