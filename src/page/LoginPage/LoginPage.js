import React from "react";
import Lottie from "lottie-react";
import bg_animate from "../../assets/lf20_CTaizi.json";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { postLogin } from "./../../service/userService";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userLocalService } from "../../service/localService";
import { SET_USER_LOGIN } from "./../../redux/constants/userConstant";
import "../../HOC/Layout.css"

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("values: ", values);
    postLogin(values)
      .then((res) => {
        console.log("res: ", res);
        message.success("Đăng nhập thành công");
        userLocalService.set(res.data.content);
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng Nhập Thất Bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <div className="h-screen w-screen  ">
      <img className="absolute bg-opacity-75  -z-10" src={bg_animate} alt="" />
      <div className="h-screen fixed opacity-90 w-screen">
        <div className=" px-10 rounded-3xl flex justify-center items-center ">
          <div className="w-1/2">
            <Lottie animationData={bg_animate} />
          </div>
          <div className="w-1/2">
            <NavLink to={"/"} className="text-2xl text-indigo-800 ml-2 font-semibold">
                  CyberMovie
                </NavLink>
            <h2
              className="text-center text-4xl text-indigo-900 font-semibold mb-10 mt-10
              "
            >
              Đăng Nhập
            </h2>
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 15,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                className="font-medium"
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Bạn chưa nhập tài khoản",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                className="font-medium"
                label="Mật Khẩu"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Bạn chưa nhập mật khẩu",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 8,
                }}
              >
                <div className="flex gap-2">
                  <Button
                    className="bg-red-500 font-medium text-white  "
                    htmlType="submit"
                  >
                    Đăng Nhập
                  </Button>
                  <Button className="bg-[#22A39F] font-medium text-white  ">
                    <NavLink to={"/register"}>Đăng Ký</NavLink>
                  </Button>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}
