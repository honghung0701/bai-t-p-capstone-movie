
import { https } from './configURL';


export const getBannerMovie = () => {
    return https.get(`/api/QuanLyPhim/LayDanhSachBanner`)
}
export const getMovieList = () => {
    return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`)
}
export const getMovieByTheate = () => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`)
}
export const getDetailMovie = (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
}
export const getMovieInfor = (maPhim) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`)
}
