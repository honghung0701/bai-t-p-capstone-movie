import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
import { userReducer } from './../../redux/reducers/userReducer';


export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });
  const handleLogout = () => {
    userLocalService.remove();

    window.location.reload();
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <span>{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng Nhập
          </button>
          <button
            onClick={() => {
              window.location.href = "/register";
            }}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng Kí
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}
