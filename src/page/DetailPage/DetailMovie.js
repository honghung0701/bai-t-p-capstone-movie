import React from "react";
import { useParams } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import { getDetailMovie } from "../../service/movieService";
import moment from "moment/moment";
import "moment/locale/vi";
import "moment";
import { Modal, Rate } from "antd";
import ReactPlayer from "react-player";
import DetailTab from "./DetailTab";

export default function DetailMovie() {
  let param = useParams();
  const [modalOpen, setModalOpen] = useState(false);
  const handleShowModal = () => {
    setModalOpen(true);
  };
  const handleOkModal = () => {
    setModalOpen(false);
  };
  const handleCancelModal = () => {
    setModalOpen(false);
  };
  const [detailMovie, setDetailMovie] = useState([]);
  useEffect(() => {
    getDetailMovie(param.id)
      .then((res) => {
        console.log(res);
        setDetailMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderDetailMovie = () => {
    return (
      <div className="flex mt-2 bg-lime-400 items-center rounded-xl">
        <div className="w-1/4">
          <img
            className=" duration-700 rounded-xl h-72"
            src={detailMovie.hinhAnh}
            alt=""
          />
        </div>
        <div className="w-3/4 mt-4">
          <h3 className="font-medium">Tên phim: {detailMovie.tenPhim}</h3>
          <p className="font-medium">Ngày khởi chiếu: </p>{" "}
          {moment(detailMovie.ngayKhoiChieu).format("LLL")}
          <p className="font-medium">Mô tả: </p> {detailMovie.moTa}
          <div className="space-x-3">
            <button
              className="bg-sky-500 hover:bg-sky-700 font-medium text-sm p-2 rounded-xl mt-2 text-white"
              onClick={handleShowModal}
            >
              Xem Trailer
            </button>
            <button className="bg-sky-500 hover:bg-sky-700 font-medium text-sm p-2 rounded-xl mt-2 text-white">
              Mua Vé
            </button>
            <Modal
              width="700px"
              height="400px"
              title="Trailer Phim"
              open={modalOpen}
              onOk={handleOkModal}
              onCancel={handleCancelModal}
            >
              <ReactPlayer
                width="600px"
                height="300px"
                url={detailMovie.trailer}
              />
            </Modal>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div className="container mx-auto">
      <h2 className="mt-10 text-xl font-semibold ">Nội dung phim</h2>
      <hr />
      {renderDetailMovie()}
      <h2 className="mt-10 text-xl font-semibold ">Lịch chiếu phim</h2>
      <hr />
      <DetailTab param={param} />
    </div>
  );
}
