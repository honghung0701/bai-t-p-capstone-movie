import React from "react";
import { Card } from "antd";
import { useState } from "react";
import { useEffect } from "react";
import { getMovieList } from "../../../service/movieService";
import { NavLink } from "react-router-dom";
import { Pagination } from "antd";

const { Meta } = Card;

export default function MovieList() {
  // state = {
  //   minIndex: 0,
  //   maxIndex: 1
  // };

  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    getMovieList()
      .then((res) => {
        console.log(res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // const pageSize = 12;

  // const handleChange = (page) => {
  //   this.setState({
  //     minIndex: (page - 1) * pageSize,
  //     maxIndex: page * pageSize,
  //   });
  // };
  const renderMovieList = () => {
    return movieArr.map((item) => {
      return (
        <Card
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <div style={{ overflow: "hidden", height: "300px" }}>
              <img className="h-full w-full" alt="example" src={item.hinhAnh} />
            </div>
          }
        >
          <Meta
            title={
              <h2 className="text-black-700 h-10 font-semibold">
                {item.tenPhim}
              </h2>
            }
          />
          <NavLink
            className="bg-red-600 px-5 py-2 text-white rounded"
            to={`/detail/${item.maPhim}`}
          >
            Xem chi tiết
          </NavLink>
          <p className="mt-5">{item.moTa.slice(0, 30)}...</p>
        </Card>
      );
    });
  };

  return (
    <div className="container mx-auto px-20 mt-10 ">
      <div className="mb-5">
        <h1 className="font-semibold text-2xl mb-2 text-zinc-500 ">
          Danh Sách Phim
        </h1>
        <hr />
      </div>
      <div className="grid grid-cols-4 gap-7">{renderMovieList()}</div>
      <Pagination className="my-5 text-center" defaultCurrent={1} total={4} />
    </div>
  );
}
