import React, { useEffect, useState } from "react";
import CarouselHome from "./Carousel/CarouselHome";
import MovieList from "./MovieList/MovieList";
import MovieTab from "./MovieTab/MovieTab";
import { getBannerMovie } from "../../service/movieService";

export default function HomePage() {
  const [imgArr, setImgArr] = useState([]);
  useEffect(() => {
    getBannerMovie()
      .then((res) => {
        console.log(res);
        setImgArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <CarouselHome imgArr={imgArr} />
      <MovieList />
      <MovieTab />
    </div>
  );
}
