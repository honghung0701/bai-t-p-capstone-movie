import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { getMovieByTheate } from "./../../../service/movieService";
import MovieItemTab from "./MovieItemTab";

const onChange = (key) => {
  console.log(key);  
};
export default function MovieTab() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    getMovieByTheate()
      .then((res) => {
        console.log(res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderDanhSachPhimTheoCumRap = (cumRap) => {
    return cumRap.danhSachPhim.map((movie) => {
      return <MovieItemTab movie={movie}/>
    })
  }
  const renderCumRapTheoHeThongRap = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap) => {
      return {
        label: (
          <div className="w-44">
            <h3>{cumRap.tenCumRap}</h3>
            <p className="truncate">{cumRap.diaChi}</p>
            <p className="text-red-600">[Chi Tiết]</p>
          </div>
        ),
        key: cumRap.maCumRap,
        children: (
          <div style={{height: 400, overflowY: "scroll"}}>{renderDanhSachPhimTheoCumRap(cumRap)}</div>
        )
      }
    })
  };
  const renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThongRap(heThongRap)}
          />
        ),
      };
    });
  };
  return (
    <div className="container mx-auto px-20 py-10">
      <div className="mb-5">
        <h1 className="font-semibold text-2xl mb-2 text-zinc-500 ">Rạp Chiếu</h1>
        <hr />
      </div>
      <Tabs
        style={{
          height: 400,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
