import React from "react";
import { Carousel } from "antd";

export default function CarouselHome({ imgArr }) {
  const contentStyle = {
    height: "600px",
    color: "#fff",
    lineHeight: "100px",
    textAlign: "center",
    backgroundSize: "100%",
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat"
  };
  const renderBannerMovie = () => {
    return imgArr.map((item, index) => {
      return (
        <div key={index}>
          <div style={{...contentStyle, backgroundImage: `url(${item.hinhAnh})`}}> 
          <img className="w-full opacity-0" src={item.hinhAnh} alt="" />;
        </div>
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderBannerMovie()}</Carousel>;
}
