import React from "react";
import Header from "./../component/Header/Header";
import Footer from "./../component/Footer/Footer";
// import "./Layout.css";

export default function Layout({ children }) {
  return (
    <div>
      <div>
        <Header />
        {children}
        <Footer />
      </div>
    </div>
  );
}
